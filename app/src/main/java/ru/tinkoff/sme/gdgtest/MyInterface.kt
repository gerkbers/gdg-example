package ru.tinkoff.sme.gdgtest

interface MyInterface {

    val a: Int

    companion object {
        operator fun invoke(a: Int): MyInterface {
            return Implem(a)
        }
    }

    operator fun plus(b: MyInterface): MyInterface = MyInterface(a + b.a)
}

data class Implem(override val a: Int) : MyInterface {

}
