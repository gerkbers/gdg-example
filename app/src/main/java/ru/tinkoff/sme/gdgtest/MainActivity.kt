package ru.tinkoff.sme.gdgtest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Patterns.WEB_URL
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.TimeUnit
import androidx.core.app.ComponentActivity
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import java.util.*


class MainActivity : AppCompatActivity() {

    private lateinit var value: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        Collections.sort()
        task3()
    }

    fun task1() {
        val sb = StringBuilder()
        Observable.interval(500, TimeUnit.MILLISECONDS)
                .take(5)
                .startWith{35}
                .switchMap {period ->
                    Observable.create<Long>{
                        it.onNext(period)
                        it.onError(IllegalStateException("Illegal state"))
                        it.onComplete()
                    }
                            .onErrorReturn { -1 }
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({

                    Log.d("Task1", "$it")
                }, {

                },
                        {
                            result.text = sb
                        })
    }

    fun task2() {
        val sb = StringBuilder()
        val observable = Observable.create<Int> {
            for(i in 0..5) {
                it.onNext(i)
            }
            //it.onComplete()
        }
        Observable.range(1, 3)
                .concatMap { observable }
                .subscribe({
                    Log.d("Task2", "$it")
                }, {

                },
                        {

                        })



        val b = MyInterface(4) + MyInterface(5)
    }

    private val URI_REGEX = "\\w+://[^\\s]+"

    fun task3() {
        Log.d("Task3", WEB_URL.matcher("google").matches().toString())
    }
}
